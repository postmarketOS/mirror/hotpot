
pub mod config;
pub mod git;
pub mod mqtt;

#[derive(Default)]
pub struct RepoState {
    pub commit: String,
}
