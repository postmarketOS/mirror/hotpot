use git2::{self, Repository};
use anyhow::Result;
use std::{fs, io::{self, Write}, path::PathBuf};

use crate::config;

pub struct HpGit {
    aports: Repository,
    pmaports: Repository,
    pmaports_branch: String,
}

impl HpGit {
    pub fn init(config: &config::Config) -> Result<Self> {    
        let aports_dir = config.workdir.join("aports_upstream");
        let pmaports_dir = config.workdir.join("pmaports");

        fs::create_dir_all(&aports_dir)?;
        fs::create_dir(&pmaports_dir)?;

        let aports = init_repo(&config.aports_url, &aports_dir, "master")?;
        let pmaports = init_repo(&config.pmaports_url, &pmaports_dir, &config.pmaports_branch)?;

        Ok(HpGit {
            aports,
            pmaports,
            pmaports_branch: config.pmaports_branch.clone(),
        })
    }

    pub fn aports_head(&self) -> Result<String> {
        let head = self.aports.head()?;
        let oid = head.target().unwrap();
        let commit = self.aports.find_commit(oid)?;
        Ok(commit.id().to_string())
    }

    pub fn pmaports_pull(&self) -> Result<()> {
        let mut fo = git2::FetchOptions::new();
        fo.download_tags(git2::AutotagOption::Auto);

        let mut remote = self.pmaports.find_remote("origin")?;
        remote.fetch(&[&self.pmaports_branch], Some(&mut fo), None)?;

        let head = self.pmaports.head()?;
        let head_oid = head.target().unwrap();
        let head_commit = self.pmaports.find_commit(head_oid)?;

        /* FIXME: finish this later...
         * https://github.com/rust-lang/git2-rs/blob/master/examples/pull.rs
         */
        // self.pmaports.
        // self.pmaports.merge_commits(&head_commit, &head_commit, Some(&mut fo))?;

        Ok(())
    
    }
}

fn init_repo(url: &str, path: &PathBuf, branch: &str) -> Result<git2::Repository> {
    match Repository::open(&path) {
        Ok(repo) => Ok(repo),
        Err(_) => {
            let mut builder = git2::build::RepoBuilder::new();
            let mut fo = git2::FetchOptions::new();
            let mut cb = git2::RemoteCallbacks::new();

            // Print out our transfer progress.
            cb.transfer_progress(|stats| {
                if stats.received_objects() == stats.total_objects() {
                    print!(
                        "Resolving deltas {}/{}\r",
                        stats.indexed_deltas(),
                        stats.total_deltas()
                    );
                } else if stats.total_objects() > 0 {
                    print!(
                        "Received {}/{} objects ({}) in {} bytes\r",
                        stats.received_objects(),
                        stats.total_objects(),
                        stats.indexed_objects(),
                        stats.received_bytes()
                    );
                }
                io::stdout().flush().unwrap();
                true
            });

            fo.remote_callbacks(cb);

            /* We clone with depth 1 because we only care about the state NOW
            * and going forwards, not the history */
            fo.depth(1);

            builder.fetch_options(fo);
            builder.branch(branch);

            println!("[GIT] Cloning '{}' into {:?}", url, path);
            let repo = builder.clone(url, &path)?;
            println!();
            Ok(repo)
        }
    }
}
